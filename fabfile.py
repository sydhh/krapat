#import os
from pathlib import Path

# from dotenv import load_dotenv
import environ
from fabric import task, Connection


@task
def upload_image(ctx, problem, image, optim="", resized=None, dryrun=None):
    """
    Task to upload an image, following the steps:
    1- backup original image (original jpeg image, no changes)
    2- import image optimised + resized versions in db + media
    """

    # args
    dry_run = False
    if dryrun:
        print("Using dry run mode when running import_image script")
        dry_run = True

    # reading .env file
    env = environ.Env()
    env_filepath = "env/deploy_prod.env"
    print(f"Reading {env_filepath}")
    environ.Env.read_env(env_file=env_filepath)
    
    conn = env("connection")
    venv_folder = env("venv_folder")
    repo_git = env("repo_git")
    images_backup_folder = env("images_backup_folder")
    images_tmp_folder = env("images_tmp_folder")
    
    # TODO: display recap: image names + size
    # TODO: prevent image overwrite
    
    orig_remote_path = str(Path(images_backup_folder) / Path(image).name)
    optim_remote_path = str(Path(images_tmp_folder) / Path(optim).name) 

    resized_split = resized.split(",")
    resized_remote_pathes_ = []
    for img in resized_split:
        resized_remote_path = str(Path(images_tmp_folder) / Path(img).name)
        resized_remote_pathes_.append(resized_remote_path)
    resized_remote_pathes = " ".join(resized_remote_pathes_)

    with Connection(conn) as c:
        
        # step 1: bak original image
        print(f"Copying image: {image} to remote path: {orig_remote_path}...")
        c.put(image, orig_remote_path)
        print(f"Done.")

        # step 2: copy image optimized + resized versions to remote tmp folder 
        print(f"Copying image: {optim} to remote temp path: {optim_remote_path}...")
        c.put(optim, optim_remote_path)
        print(f"Done.")

        for resized_img, resized_img_remote in zip(resized_split, resized_remote_pathes_):
            print(f"Copying image: {resized_img} to remote temp path: {resized_img_remote}...")
            c.put(resized_img, resized_img_remote)
            print(f"Done.")

        # step 3: import into web site / db
        print("Now importing images into www...")
        with c.cd(f"{repo_git}/backend"):
            cmd = f"""{venv_folder}/python ./manage.py import_image \
                --settings backend.settings_prod \
                --problem '{problem}' \
                --image {optim_remote_path} \
                --resized {resized_remote_pathes} \
                """
            if dry_run:
                cmd += " --dry_run"
            c.run(cmd)
        
        # step 4: cleaning images in tmp
        c.run(f"rm -v {optim_remote_path}")
        for img in resized_remote_pathes_:
            c.run(f"rm -v {img}")

@task
def deps(ctx):
    """
    Task to list dependencies
    """

    # reading .env file
    env = environ.Env()
    env_filepath = "env/deploy_prod.env"
    print(f"Reading {env_filepath}")
    environ.Env.read_env(env_file=env_filepath)

    venv_folder = env("venv_folder")
    conn = env("connection")

    print("Running pip list remotely...")
    with Connection(conn) as c:
        c.run(f"{venv_folder}/pip list")
