import argparse
from pathlib import Path
import re
import subprocess

if __name__ == "__main__":

    # Tool to resize and optim images
    # TODO: print human readable size + size gains

    # Build mozjpeg (Ubuntu)
    # Download last source from: https://github.com/mozilla/mozjpeg/releases
    # 
    # sudo apt install cmake build-essential nasm zlib1g-dev libpng-dev
    # tar cvzf mozjpeg-4.0.3.tar.gz
    # mkdir build && cd build
    # cmake ../ --install-prefix=/home/USER/mozjpeg_build -DPNG_SUPPORTED=ON
    #

    # Useful imagemagick command

    # How to check if jpeg + progressive
    # identify -verbose ../../data/kerlouan/crémiou_1/crémiou_1_alien_1_optim.jpg | grep Interlace
    # None -> non progressive

    # How to check exif

    # identify - verbose | grep exif

    # Convert to avif

    # identify -list format | grep AVIF
    # convert -verbose image.jpg -auto-orient image.avif

    parser = argparse.ArgumentParser(description="")

    parser.add_argument("--image", type=str, help="image path")
    parser.add_argument("--cjpeg", type=str, help="mozjpeg cjpeg binary path",
                        default="cjpeg")
    parser.add_argument("--cjpeg_quality", type=int, help="jpeg quality",
                        default=75)
    parser.add_argument("--convert", type=str, help="imagemagick convert binary path",
                        default="convert")
    parser.add_argument("--resize", type=int, nargs="+",
                        default=[1080, 720],
                        help="Resize image to given width(s)")

    args = parser.parse_args()

    image_folder = Path(args.image).parent
    image_ext_ = Path(args.image).suffixes  # [".tar", ".gz"]
    image_ext = "".join(image_ext_)  # ".tar.gz"
    image_name_no_ext = Path(args.image).name[:-len(image_ext)]
    image_optim = image_folder / (image_name_no_ext + f"_optim" + image_ext)

    subprocess.run(f"{args.cjpeg} -quality 75 -quant-table 2 {args.image} > {image_optim}",
                   shell=True)
    print(f"Optim: {image_optim}")

    for width in args.resize:
        image_resized = image_folder / (image_name_no_ext + f"_w{width}" + image_ext)
        print(f"[Resized (width: {width})]", image_resized)
        image_resized_optim = image_folder / (image_name_no_ext + f"_w{width}_optim" + image_ext)
        print(f"[Optim + resized (width: {width})]", image_resized_optim)

        # resize using imagemagick - keep image aspect ratio
        subprocess.run(f"{args.convert} {args.image} -resize {width}x {image_resized}",
                       shell=True)
        # optim image with mozjpeg
        subprocess.run(f"{args.cjpeg} -quality 75 -quant-table 2 {image_resized} > {image_resized_optim}",
                       shell=True)