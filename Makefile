ENV_VARS = $(shell cat env/web_dev.env | xargs)
mypy:
	cd backend && env $(ENV_VARS) ../venv/bin/pipenv run mypy --config-file ../.mypy.ini .

req:
	venv/bin/pipenv requirements > requirements.txt

req_dev:
	venv/bin/pipenv requirements --dev > requirements_dev.txt

format:
	venv/bin/pipenv run black backend/