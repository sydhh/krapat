## How to contribute to Krapat

#### **Did you find a bug?**

* **Ensure the bug was not already reported** by searching on Gitlab under [Issues](https://gitlab.com/sydhh/krapat/issues).

* If you're unable to find an open issue addressing the problem, [open a new one](https://gitlab.com/sydhh/krapat/issues/new). Be sure to include a **title and clear description**, as much relevant information as possible, and a **code sample** or an **executable test case** demonstrating the expected behavior that is not occurring.

#### **Did you write a patch that fixes a bug?**

* Open a new Gitlab merge request with the patch.

* Ensure the PR description clearly describes the problem and solution. Include the relevant issue number if applicable.

#### **Did you fix whitespace, format code, or make a purely cosmetic patch?**

Changes that are cosmetic in nature and do not add anything substantial to the stability, functionality, or testability will generally not be accepted.

#### **Do you intend to add a new feature or change an existing one?**

* Suggest your change in the [Discord dev channel](TODO) and start writing code.

* Do not open an issue on Gitlab until you have collected positive feedback about the change. Gitlab issues are primarily intended for bug reports and fixes.

#### **Do you have questions about the source code?**

* Ask any question about how to use Krapat in the [Discord dev channel](TODO).

#### **Do you want to contribute to the Krapat documentation?**

* Please ask in the [Discord doc channel](TODO).

Thanks! :heart: :heart: :heart:

Krapat Team