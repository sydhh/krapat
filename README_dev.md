## Setup

* Install pipenv in a virtualenv
  * python3 -m venv venv
  * venv/bin/python -m pip install pipenv


* cd env
* cp -v web_prod.env.sample web_dev.env
* [EDIT] web_dev.env

## Run (local)

* venv/bin/pipenv shell
* cd backend
* python ./manage.py runserver --settings backend.settings_dev

* python ./manage.py makemigrations --settings backend.settings_dev krapat
* python ./manage.py migrate --settings backend.settings_dev

* python ./manage.py help --settings backend.settings_dev

## Other django run

* python ./manage.py shell --settings backend.settings_dev 
  *from krapat.models import Problem
  * Problem.objects.all().remove()

## Packages

### Update package version

* venv/bin/pipenv update

### Generate requirements.txt or requirements_dev.txt

* make req
* make req_dev

### Format code (using black)

* make format

### Check code (using mypy)

* make mypy
