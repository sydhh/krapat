from django.views import generic
from django.shortcuts import render
from django.conf import settings
from django.db.models import Count

from .models import Area, SubArea, Problem, Image


def index(request):
    areas = []
    if settings.KRAPAT_AREAS_ON_INDEX_PAGE:
        areas = Area.objects.filter(pk__in=settings.KRAPAT_AREAS_ON_INDEX_PAGE)

    return render(request, "krapat/index.html", {"areas": areas})


class AreasView(generic.ListView):
    model = Area
    template_name = "krapat/areas.html"
    context_object_name = "areas"


class AreaView(generic.ListView):
    model = SubArea
    template_name = "krapat/area.html"
    context_object_name = "subareas"

    def get_queryset(self):
        subareas = (
            SubArea.objects.filter(area__id=self.kwargs["pk"], enabled=True)
            .annotate(Count("problems"))
            .select_related("area")
        )
        return subareas


class SubAreaView(generic.ListView):
    model = Problem
    template_name = "krapat/subarea.html"
    context_object_name = "problems"

    def get_queryset(self):
        problems = Problem.objects.filter(subarea__id=self.kwargs["pk"])
        return problems


class ProblemView(generic.DetailView):
    model = Problem
    template_name = "krapat/problem.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        # Original image is the first one
        # FIXME: this assumes that order is valid for an image and its resized version
        images = Image.objects.filter(problem=context["problem"]).order_by(
            "order", "original", "-width", "-height"
        )

        # Notes: here we have all images in db like
        #        image1, image 1 resized 1, .. image 1 resized N, image 2, image 2 resized 1 ...
        # src: image1, image 2
        # srcset: image1 resized N {WIDTH}w .. image1 resized 1 {WIDTH}w image 1 {WIDTH}w image2 resized N {WIDTH}w ...

        src = []
        srcset = []
        srcset_ = []
        for img in images:
            if img.original is None:
                if srcset_:
                    srcset.append(",".join(reversed(srcset_)))
                    srcset_ = []
                src.append(img.image.url)

            srcset_.append(f"{img.image.url} {img.width}w")

        if srcset_:
            srcset.append(",".join(reversed(srcset_)))

        # FIXME: remove context["images"] and cleanup template file
        # context["images"] = Image.objects.filter(problem=context["problem"])
        context["images"] = src
        context["src_iterator"] = zip(src, srcset)

        return context


class AboutView(generic.TemplateView):
    template_name = "krapat/about.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["discord_url"] = settings.DISCORD_URL
        return context


class ContribView(generic.TemplateView):
    template_name = "krapat/contrib.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["repo_url"] = settings.REPO_URL
        context["discord_url"] = settings.DISCORD_URL
        return context
