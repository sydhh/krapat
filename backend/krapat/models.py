from django.contrib.auth.models import User
from django.db import models

# from django.contrib.gis.db import models as gis_models


class Area(models.Model):

    """A climbing area e.g. Kerlouan

    An Area can have multiple SubArea
    """

    title = models.CharField(max_length=150)
    department = models.CharField(max_length=120)
    region = models.CharField(max_length=120)
    country = models.CharField(max_length=120)
    # TODO
    # longitude = models.DecimalField(max_digits=9, decimal_places=6)
    # latitude = models.DecimalField(max_digits=9, decimal_places=6)

    def __str__(self):
        return "{}: {}".format(self.pk, self.title)


class SubArea(models.Model):

    """A climbing sub area e.g. Crémiou 1

    A SubArea is a children of an Area
    """

    title = models.CharField(max_length=150)
    # rocks = models.ForeignKey(to=Rock, on_delete=models.CASCADE)
    area = models.ForeignKey(to=Area, related_name="subareas", on_delete=models.CASCADE)
    enabled = models.BooleanField(default=False)

    def __str__(self):
        return "{}: {}".format(self.pk, self.title)


# class Parking(models.Model):
#
#     """ A parking to access a given sub area"""
#
#     # TODO: same Parking for multiple subarea?
#
#     title = models.CharField(max_length=150)
#     subarea = models.ForeignKey(to=SubArea, related_name="parkings", on_delete=models.CASCADE, null=True, blank=True)
#
#     def __str__(self):
#         return self.title


# class Rock(models.Model):
#
#     """ One rock in a subarea, there can be multiple problems on a rock"""
#
#     title = models.CharField(max_length=150)
#     # FIXME: better field name
#     n = models.IntegerField()
#     subarea = models.ForeignKey(to=SubArea, related_name="rocks", on_delete=models.CASCADE)
#
#     def __str__(self):
#         return self.title


class CircuitColor(models.IntegerChoices):
    # Fontainebleau circuit colors
    WHITE = 0, "White"
    YELLOW = 1, "Yellow"
    ORANGE = 2, "Orange"
    BLUE = 3, "Blue"
    RED = 4, "Red"
    BLACK = 5, "Black"
    PURPLE = 6, "Purple"


# class Circuit(models.Model):
#     title = models.CharField(max_length=150)
#     color = models.IntegerField(choices=CircuitColor.choices, default=CircuitColor.BLUE)
#     # TODO: ?
#     # children = models.BooleanField(default=False)
#     subarea = models.ForeignKey(to=SubArea, related_name="circuits", on_delete=models.CASCADE)


# class Variant(models.Model):
#     rock = models.ForeignKey(to=Rock, related_name="variants", on_delete=models.CASCADE)


class ProblemGrade(models.IntegerChoices):
    G_UNKNOWN = 0, "?"
    G_2 = 1, "2"
    G_2A = 2, "2A"
    G_2B = 3, "2B"
    G_2C = 4, "2C"
    G_3 = 5, "3"
    G_3A = 6, "3A"
    G_3B = 7, "3B"
    G_3C = 8, "3C"
    G_4 = 9, "4"
    G_4A = 10, "4A"
    G_4B = 11, "4B"
    G_4C = 12, "4C"
    G_5 = 13, "5"
    G_5A = 14, "5A"
    G_5B = 15, "5B"
    G_5C = 16, "5C"
    G_6 = 17, "6"
    G_6A = 18, "6A"
    G_6B = 19, "6B"
    G_6C = 20, "6C"
    G_7 = 21, "7"
    G_7A = 22, "7A"
    G_7B = 23, "7B"
    G_7C = 24, "7C"
    G_8 = 25, "8"
    G_8A = 26, "8A"
    G_8B = 27, "8B"
    G_8C = 28, "8C"
    G_9 = 29, "9"
    G_9A = 30, "9A"
    G_9B = 31, "9B"
    G_9C = 32, "9C"


class ProblemGradeAddon(models.IntegerChoices):
    G_MINUS = 0, "-"
    G_PLUS = 1, "+"


# def properties_defaults():
#
#     return {
#         "sit_start": False,
#         "spotter_required": True,
#         "dyno": False
#     }


class Problem(models.Model):
    # fields
    title = models.CharField(max_length=150)
    description = models.TextField(null=True)
    order = models.PositiveIntegerField(null=True, blank=True)
    # published = models.BooleanField(default=False)
    # created_by = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    # variants = models.ForeignKey(to="Problem", on_delete=models.SET_NULL)
    # variant = models.ForeignKey("self")
    # data = models.JSONField(null=True)
    subarea = models.ForeignKey(
        to=SubArea, related_name="problems", on_delete=models.CASCADE
    )
    # rock = models.ForeignKey(to=Rock, related_name="problems", null=True, blank=True, on_delete=models.CASCADE)
    # circuit = models.ForeignKey(to=Circuit, related_name="problems", null=True, blank=True, on_delete=models.CASCADE)
    # variant = models.ForeignKey(to=Variant, related_name="problems", null=True, blank=True, on_delete=models.CASCADE)

    grade = models.IntegerField(
        choices=ProblemGrade.choices, default=ProblemGrade.G_UNKNOWN
    )
    grade_addon = models.IntegerField(
        choices=ProblemGradeAddon.choices, default=None, null=True, blank=True
    )

    # problem properties
    # properties = models.JSONField(null=True, default=properties_defaults)

    def __str__(self):
        return "{}: {} - {}".format(
            self.pk,
            self.title,
            self.description
            if len(self.description) < 20
            else self.description[:20] + "...",
        )


class Image(models.Model):
    # title = models.CharField(max_length=150, null=True, blank=True)
    image = models.ImageField(upload_to="images")
    problem = models.ForeignKey(
        to=Problem,
        related_name="images",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    # rock = models.ForeignKey(to=Rock, related_name="images",
    #                          null=True, blank=True,
    #                          on_delete=models.CASCADE)

    width = models.PositiveIntegerField(default=0)
    height = models.PositiveIntegerField(default=0)
    # kind = ... # enum ?
    original = models.ForeignKey(
        to="self",
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING,
        related_name="org",
    )
    order = models.PositiveIntegerField(default=0)

    def __str__(self):
        s = f"Image (id: {self.pk}) (size: {self.width}/{self.height}) for problem: {self.problem_id}"
        if self.original:
            s += f", original image: {self.original.pk}"

        return s
