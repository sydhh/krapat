from django import forms

from .models import Area, SubArea, Problem, Image


class ProblemCreateForm(forms.ModelForm):
    class Meta:
        model = Problem
        # exclude = ("created_by",)
        labels = {"title": "Title"}
        fields = (
            "__all__"  # Or a list of the fields that you want to include in your form
        )


class AreaCreateForm(forms.ModelForm):
    class Meta:
        model = Area
        # exclude = ("created_by",)
        labels = {"title": "Title"}
        fields = (
            "__all__"  # Or a list of the fields that you want to include in your form
        )


class SubAreaCreateForm(forms.ModelForm):
    class Meta:
        model = SubArea
        # exclude = ("created_by",)
        labels = {"title": "Title"}
        fields = (
            "__all__"  # Or a list of the fields that you want to include in your form
        )


class ImageForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ("image", "problem")  # "title",
