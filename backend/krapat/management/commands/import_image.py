from pathlib import Path

from django.core.management import BaseCommand
from PIL import Image as PIL_Image

from krapat.models import Area, SubArea, Problem, Image


class Command(BaseCommand):
    help = "Import an image (and it resized versions) for a Problem"

    def add_arguments(self, parser):  # pylint: disable=no-self-use
        """
        Add argument to the  command
        :param parser: Argument parser
        :type parser: ArgumentParser
        :return: None
        """

        parser.add_argument(
            "--problem", type=str, help="import image for given problem (Title)"
        )
        # parser.add_argument("--parking", type=str, help="import image for given parking (Title)")
        parser.add_argument("--image", type=str, help="image path to import")
        # parser.add_argument("--title", type=str, help="image title")
        parser.add_argument(
            "--resized",
            type=str,
            nargs="+",
            help="Resized images to import, format: --resized foo_w600.jpg foo_w300.jpg",
        )
        parser.add_argument(
            "--dry_run",
            action="store_true",
            required=False,
            help="Do not import anything, only print things to migrate",
        )

    def handle(self, *args, **options):
        # print("import", args, options)
        # print("===")

        image_path = options["image"]
        if not Path(image_path).is_file():
            raise ValueError("Please provide a valid image path")

        if options["problem"]:
            # TODO: problem id
            p = Problem.objects.filter(title=options["problem"])
            if not p:
                raise ValueError("Could not find problem:", options["problem"])
            if len(p) > 2:
                raise ValueError(
                    f"More than one problem found with name: {options['problem']}"
                )
            p = p[0]

        images_existing = Image.objects.filter(problem=p)
        if images_existing:
            raise RuntimeError(f"Found {len(images_existing)} existing images for problem: {p.title}")

        with open(image_path, "rb") as fp:
            img = PIL_Image.open(fp)
            image = Image(
                # title=options["title"],
                problem=p,
                width=img.width,
                height=img.height,
            )

            if not options["dry_run"]:
                image.image.save(Path(image_path).name, fp, True)

        if options["dry_run"]:
            print(f"[DRYRUN] Importing image: {image_path} done!")
        else:
            print(f"Importing image: {image_path} done (Image id: {image.pk})!")

        if options["resized"]:
            for image_resized_path in options["resized"]:
                print(f"Processing {image_resized_path}...")
                with open(image_resized_path, "rb") as fp:
                    img = PIL_Image.open(fp)
                    image_resized = Image(
                        problem=p, original=image, width=img.width, height=img.height
                    )
                    if not options["dry_run"]:
                        image_resized.image.save(
                            Path(image_resized_path).name, fp, True
                        )

                if options["dry_run"]:
                    print(f"[DRYRUN] Importing resized image: {image_resized_path} done!")
                else:
                    print(
                        f"Importing resized image: {image_resized_path} done (Image id: {image_resized.pk})!"
                    )
