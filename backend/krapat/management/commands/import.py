import csv
import re

from django.core.management import BaseCommand

from krapat.models import Area, SubArea, Problem, ProblemGrade, ProblemGradeAddon

def area_handle_row(row, options):

    # build a dict from csv row
    area_dict = {k.lower(): v for k, v in row.items()}

    # check if Area does not exist yet
    area_exist = Area.objects.filter(title=area_dict["title"])

    if area_exist:
        raise NotImplementedError()
    else:
        a = Area(**area_dict)
        if not options["dryrun"]:
            a.save()
            row["Id"] = a.id
            print(f"Created new area (id: {a.id}): {a.title}")
        else:
            print(f"[DRYRUN] Create new area: {a.title}")

def subarea_handle_row(row, options):

    # query area
    a = Area.objects.filter(title=options["area"])

    if not a:
        raise RuntimeError("Cannot find area: {}", options["area"])

    if len(a) > 1:
        raise RuntimeError("More than one area with title: {}", options["area"])

    # build a dict from csv row
    subarea_dict = {k.lower(): v for k, v in row.items()}

    # check if Area does not exist yet
    subarea_exist = SubArea.objects.filter(title=subarea_dict["title"])

    if subarea_exist:
        raise NotImplementedError()
    else:
        sa = SubArea(**subarea_dict)
        sa.area = a[0]
        if not options["dryrun"]:
            sa.save()
            row["Id"] = sa.id
            print(f"Created new subarea (id: {sa.id}): {sa.title}")
        else:
            print(f"[DRYRUN] Create new subarea: {sa.title}")

def problem_handle_row(row, options):

    # First query area
    a = Area.objects.filter(title=options["area"])

    if not a:
        raise RuntimeError("Cannot find area: {}", options["area"])

    if len(a) > 1:
        raise RuntimeError("More than one area with title: {}", options["area"])
    
    # then query sub area
    sa = SubArea.objects.filter(title=options["subarea"], area=a[0])

    if not sa:
        subarea_avail_ = SubArea.objects.filter(area=a[0])
        subarea_avail = [sa.title for sa in subarea_avail_]
        raise RuntimeError("Cannot find subarea: {}, availabe: {}", options["subarea"], subarea_avail)

    if len(sa) > 1:
        raise RuntimeError("More than one subarea with title: {}", options["subarea"])

    # build a dict from csv row
    problem_dict = {k.lower(): v for k, v in row.items() if k != "Grade"}

    # check if Area does not exist yet
    problem_exist = Problem.objects.filter(title=problem_dict["title"], order=problem_dict["order"])

    grade_ = ProblemGrade.G_UNKNOWN
    grade_addon_ = None
    grades = [g[1] for g in ProblemGrade.choices]
    grade_addons = [g[1] for g in ProblemGradeAddon.choices]

    if row["Grade"]:
        grade_row = row["Grade"]
        if grade_row.endswith("-") or grade_row.endswith("+"):
            grade_ = ProblemGrade(grades.index(grade_row[:-1].upper()))
            grade_addon_ = ProblemGradeAddon(grade_addons.index(grade_row[-1].upper()))
        else:
            grade_ = ProblemGrade(grades.index(grade_row.upper()))

    if problem_exist:
        raise NotImplementedError()
    else:
        p = Problem(**problem_dict)        
        p.area = a[0]
        p.subarea = sa[0]
        p.grade = grade_
        p.grade_addon = grade_addon_
        if not options["dryrun"]:
            p.save()
            row["Id"] = p.id
            print(f"Created new problem (id: {p.id}): {p.title}")
        else:
            print(f"[DRYRUN] Create new problem: {p.title}")

class Command(BaseCommand):
    help = "Import csv (Area, SubArea, Problem)"

    def add_arguments(self, parser):  # pylint: disable=no-self-use
        """
        Add argument to the  command
        :param parser: Argument parser
        :type parser: ArgumentParser
        :return: None
        """

        kind_choices = ("area", "subarea", "problem")
        parser.add_argument(
            "--kind",
            choices=kind_choices,
            default=kind_choices[0],
            help="import for given csv file",
        )
        parser.add_argument("--csv", type=str, help="csv file path to read")
        parser.add_argument("--area", type=str, help="Area title when importing sub area, problem...")
        parser.add_argument("--subarea", type=str, help="SubArea title when importing problem...")
        parser.add_argument(
            "--dryrun",
            action="store_true",
            required=False,
            help="Do not delete anything, only print things to migrate",
        )

    def handle(self, *args, **options):
        with open(options["csv"], "r") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if options["kind"] == "area":
                    area_handle_row(row, options)
                elif options["kind"] == "subarea":
                    subarea_handle_row(row, options)
                elif options["kind"] == "problem":
                    problem_handle_row(row, options)

