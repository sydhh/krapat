import environ

# env

env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)

# reading .env file
environ.Env.read_env(env_file="../env/web_prod.env")

from .settings import *  # noqa
