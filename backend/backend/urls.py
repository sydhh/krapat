from django.contrib import admin
from django.urls import path
from django.conf import settings

from krapat import views as kviews


urlpatterns = [
    path("", kviews.index, name="index"),
    path("about", kviews.AboutView.as_view(), name="about"),
    path("contrib", kviews.ContribView.as_view(), name="contrib"),
    # area
    path("areas", kviews.AreasView.as_view(), name="areas"),
    path("area/<int:pk>", kviews.AreaView.as_view(), name="area"),
    # subarea
    path("subarea/<int:pk>", kviews.SubAreaView.as_view(), name="subarea"),
    # problem
    path("problem/<int:pk>", kviews.ProblemView.as_view(), name="problem"),
    # parking
    # path("parking/<int:pk>", kviews.ParkingView.as_view(), name="parking"),
    # path("admin/", admin.site.urls),
    path(f"admin_{settings.ADMIN_URL_SUFFIX}/", admin.site.urls),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.urls import include

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    if settings.KRAPAT_ENABLE_DJANGO_DEBUG_TOOLBAR:
        urlpatterns += [
            path("__debug__/", include("debug_toolbar.urls")),
        ]
