# Krapat

[A Django website to view bouldering problems in Brittany France](https://www.krapat.fr). Feel free to contribute or join the [community](https://discord.gg/7CjZBPtSgc) !

## Goals

* Open source code & tools (always!)
* Build a community around bouldering in Brittany France

## Roadmap

* Visualize problem on image
* GPS + map
* Let users vote for problem grade, add comments, share images & videos
* Have a generic Django app reusable for other websites / locations

## Links

* [Community](https://discord.gg/7CjZBPtSgc)
* [Contributing](CONTRIBUTING.md)
* [Dev setup](README_dev.md)
* [Licence](LICENSE.txt)
* [Kerlouan csv topo](https://gitlab.com/sydhh/krapat_kerlouan)